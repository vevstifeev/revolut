CREATE TABLE account (
  id              bigint auto_increment NOT NULL PRIMARY KEY,
  amount          NUMBER(20,2)
);

CREATE TABLE transfer (
  id              bigint auto_increment NOT NULL PRIMARY KEY,
  id_from_account bigint NOT NULL,
  id_to_account   bigint NOT NULL,
  amount          NUMBER(20,2),
  timestamp       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (id_from_account) REFERENCES account(id),
  FOREIGN KEY (id_to_account) REFERENCES account(id)
);