package ru.eva.revolut.api.service.model;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;

@Mapper
public interface ToApiMapper {
    ApiAccount toApiAccount(Account account);

    @Mappings({
            @Mapping(target = "from", source = "idFromAccount"),
            @Mapping(target = "to", source = "idToAccount"),
    })
    ApiTransfer toApiTransfer(Transfer transfer);
}
