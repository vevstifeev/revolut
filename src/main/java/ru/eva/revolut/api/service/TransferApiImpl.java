package ru.eva.revolut.api.service;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import ru.eva.revolut.api.MainApiException;
import ru.eva.revolut.api.service.model.ApiAccount;
import ru.eva.revolut.api.service.model.ApiTransfer;
import ru.eva.revolut.api.service.model.ToApiMapper;
import ru.eva.revolut.api.service.model.ToApiMapperImpl;
import ru.eva.revolut.api.util.ResourceResponse;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;
import ru.eva.revolut.service.AccountService;
import ru.eva.revolut.service.ServiceFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TransferApiImpl implements TransferApi {

    private final AccountService accountService = ServiceFactory.getInstance();

    private final ToApiMapper toApiMapper = new ToApiMapperImpl();

    @Override
    public void getLastAccountTransfers(BigDecimal accountId, BigDecimal count, Handler<AsyncResult<ResourceResponse<List<ApiTransfer>>>> handler) {
        Future<ResourceResponse<List<ApiTransfer>>> result;
        try {
            List<Transfer> lastTransfers = accountService.getLastTransfers(
                    Optional.ofNullable(accountId).map(BigDecimal::longValue).orElse(null),
                    Optional.ofNullable(count).map(BigDecimal::intValue).orElse(null));
            result = Future.succeededFuture(
                    new ResourceResponse<List<ApiTransfer>>().setResponse(
                            lastTransfers.stream()
                                    .map(toApiMapper::toApiTransfer)
                                    .collect(Collectors.toList())));
        } catch (Exception e) {
            result = Future.failedFuture(new MainApiException(HttpResponseStatus.BAD_REQUEST.code(), e.getMessage()));
        }
        handler.handle(result);
    }

    @Override
    public void transfer(BigDecimal fromAccountId, BigDecimal toAccountId, BigDecimal amount, Handler<AsyncResult<ResourceResponse<ApiAccount>>> handler) {
        Future<ResourceResponse<ApiAccount>> result;
        try {
            Account updatedAccount = accountService.transfer(
                    Optional.ofNullable(fromAccountId).map(BigDecimal::longValue).orElse(null),
                    Optional.ofNullable(toAccountId).map(BigDecimal::longValue).orElse(null),
                    amount);
            result = Future.succeededFuture(
                    new ResourceResponse<ApiAccount>().setResponse(toApiMapper.toApiAccount(updatedAccount)));
        } catch (Exception e) {
            result = Future.failedFuture(new MainApiException(HttpResponseStatus.BAD_REQUEST.code(), e.getMessage()));
        }
        handler.handle(result);
    }
}
