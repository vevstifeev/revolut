package ru.eva.revolut.api.service;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import ru.eva.revolut.api.MainApiException;
import ru.eva.revolut.api.service.model.*;
import ru.eva.revolut.api.util.ResourceResponse;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.service.AccountService;
import ru.eva.revolut.service.ServiceFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class AccountApiImpl implements AccountApi {

    private final AccountService accountService = ServiceFactory.getInstance();

    private final ToApiMapper toApiMapper = new ToApiMapperImpl();

    private final ToDomainMapper toDomainMapper = new ToDomainMapperImpl();

    @Override
    public void addAccount(ApiAccount account, Handler<AsyncResult<ResourceResponse<ApiAccount>>> handler) {
        Future<ResourceResponse<ApiAccount>> result;
        try {
            Account addedAccount = accountService.addAccount(toDomainMapper.toAccount(account));
            result = Future.succeededFuture(
                    new ResourceResponse<ApiAccount>().setResponse(toApiMapper.toApiAccount(addedAccount)));
        } catch (Exception e) {
            result = Future.failedFuture(new MainApiException(HttpResponseStatus.BAD_REQUEST.code(), e.getMessage()));
        }
        handler.handle(result);
    }

    @Override
    public void getAccount(BigDecimal accountId, Handler<AsyncResult<ResourceResponse<ApiAccount>>> handler) {
        Future<ResourceResponse<ApiAccount>> result;
        try {
            Account account = accountService.getAccount(accountId.longValue())
                    .orElseThrow(() -> new IllegalStateException("account with id=" + accountId + " not found"));
            result = Future.succeededFuture(
                    new ResourceResponse<ApiAccount>().setResponse(toApiMapper.toApiAccount(account)));
        } catch (Exception e) {
            result = Future.failedFuture(new MainApiException(HttpResponseStatus.BAD_REQUEST.code(), e.getMessage()));
        }
        handler.handle(result);
    }

    @Override
    public void getAccounts(Handler<AsyncResult<ResourceResponse<List<ApiAccount>>>> handler) {
        Future<ResourceResponse<List<ApiAccount>>> result;
        try {
            List<Account> accounts = accountService.getAccounts();
            result = Future.succeededFuture(
                    new ResourceResponse<List<ApiAccount>>().setResponse(
                            accounts.stream()
                                    .map(toApiMapper::toApiAccount)
                                    .collect(Collectors.toList())));
        } catch (Exception e) {
            result = Future.failedFuture(new MainApiException(HttpResponseStatus.BAD_REQUEST.code(), e.getMessage()));
        }
        handler.handle(result);
    }
}
