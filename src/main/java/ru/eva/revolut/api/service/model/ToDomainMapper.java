package ru.eva.revolut.api.service.model;

import org.mapstruct.Mapper;
import ru.eva.revolut.domain.tables.pojos.Account;

@Mapper
public interface ToDomainMapper {
    Account toAccount(ApiAccount apiAccount);
}
