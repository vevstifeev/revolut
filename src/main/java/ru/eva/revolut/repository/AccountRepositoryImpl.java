package ru.eva.revolut.repository;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import ru.eva.revolut.domain.tables.pojos.Account;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Singleton
public class AccountRepositoryImpl implements AccountRepository {

    private Provider<EntityManager> entityManagerProvider;

    @Inject
    public AccountRepositoryImpl(Provider<EntityManager> entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    public List<Account> findAll() {
        EntityManager entityManager = entityManagerProvider.get();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> rootEntry = cq.from(Account.class);
        CriteriaQuery<Account> all = cq.select(rootEntry);
        TypedQuery<Account> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public Optional<Account> findById(Long id) {
        EntityManager entityManager = entityManagerProvider.get();
        return Optional.ofNullable(entityManager.find(Account.class, id));
    }

    @Override
    public Account save(Account account) {
        EntityManager entityManager = entityManagerProvider.get();
        entityManager.persist(account);
        return account;
    }
}
