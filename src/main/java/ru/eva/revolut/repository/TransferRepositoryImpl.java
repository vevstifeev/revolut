package ru.eva.revolut.repository;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;
import ru.eva.revolut.domain.tables.pojos.Transfer_;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Singleton
public class TransferRepositoryImpl implements TransferRepository {

    private Provider<EntityManager> entityManagerProvider;

    @Inject
    public TransferRepositoryImpl(Provider<EntityManager> entityManagerProvider) {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    public Transfer save(Transfer transfer) {
        EntityManager entityManager = entityManagerProvider.get();
        entityManager.persist(transfer);
        return transfer;
    }

    @Override
    public List<Transfer> findByAccountId(Long accountId, Integer count) {
        EntityManager entityManager = entityManagerProvider.get();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Transfer> cq = cb.createQuery(Transfer.class);
        Root<Transfer> rootEntry = cq.from(Transfer.class);
        cq.where(
                cb.or(
                        cb.equal(rootEntry.get(Transfer_.idFromAccount), accountId),
                        cb.equal(rootEntry.get(Transfer_.idToAccount), accountId)
                ));
        cq.orderBy(cb.asc(rootEntry.get(Transfer_.timestamp)));
        TypedQuery<Transfer> query = entityManager.createQuery(cq).setMaxResults(count);
        return query.getResultList();
    }
}
