package ru.eva.revolut.repository;

import ru.eva.revolut.domain.tables.pojos.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    List<Account> findAll();

    Optional<Account> findById(Long id);

    Account save(Account account);
}
