package ru.eva.revolut.repository;

import ru.eva.revolut.domain.tables.pojos.Transfer;

import java.util.List;

public interface TransferRepository {

    Transfer save(Transfer transfer);

    List<Transfer> findByAccountId(Long accountId, Integer count);

}
