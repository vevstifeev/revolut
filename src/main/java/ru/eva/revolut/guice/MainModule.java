
package ru.eva.revolut.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import ru.eva.revolut.repository.AccountRepository;
import ru.eva.revolut.repository.AccountRepositoryImpl;
import ru.eva.revolut.repository.TransferRepository;
import ru.eva.revolut.repository.TransferRepositoryImpl;
import ru.eva.revolut.service.AccountService;
import ru.eva.revolut.service.AccountServiceImpl;
import ru.eva.revolut.service.ServiceFactory;

import java.util.Properties;

public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new JpaPersistModule("REVOLUT_TEST").properties(jpaProperties()));

        requestStaticInjection(ServiceFactory.class);
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(TransferRepository.class).to(TransferRepositoryImpl.class);
        bind(AccountRepository.class).to(AccountRepositoryImpl.class);
    }

    Properties jpaProperties() {
        Properties properties = new Properties();

        properties.put("hibernate.connection.driver_class", "org.h2.Driver");
        properties.put("hibernate.connection.url", "jdbc:h2:mem:revolut");
        properties.put("hibernate.connection.username", "sa");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.connection.pool_size", "1");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.auto", "create");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        properties.put("hibernate.cache.use.query_cache", "true");
        properties.put("hibernate.cache.use_second_level_cache", "true");
        properties.put("hibernate.hikari.connectionTimeout", "2000");
        properties.put("hibernate.hikari.minimumIdle", "10");
        properties.put("hibernate.hikari.maximumPoolSize", "20");
        properties.put("hibernate.hikari.idleTimeout", "300000");

        return properties;
    }

    @Provides
    @Singleton
    HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance();
    }

}