package ru.eva.revolut.service;

import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface AccountService {

    List<Account> getAccounts();

    Optional<Account> getAccount(Long id);

    Account addAccount(Account account);

    Account transfer(Long fromAccountId, Long toAccountId, BigDecimal amount);

    List<Transfer> getLastTransfers(Long accountId, Integer count);
}
