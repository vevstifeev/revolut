package ru.eva.revolut.service;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class ServiceFactory {

    @Inject
    static Provider<AccountService> transferServiceProvider;

    public static AccountService getInstance() {
        return transferServiceProvider.get();
    }
}
