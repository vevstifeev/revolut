package ru.eva.revolut.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.eva.revolut.domain.TransferRequest;
import ru.eva.revolut.domain.TransferResult;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;
import ru.eva.revolut.repository.AccountRepository;
import ru.eva.revolut.repository.TransferRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


@Singleton
public class AccountServiceImpl implements AccountService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String ACCOUNT_LOCK_PREFIX = "account-";

    private final AccountRepository accountRepository;

    private final TransferRepository transferRepository;

    private final HazelcastInstance hazelcastInstance;

    @Inject
    public AccountServiceImpl(AccountRepository accountRepository,
                              TransferRepository transferRepository,
                              HazelcastInstance hazelcastInstance) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
        this.hazelcastInstance = hazelcastInstance;
    }

    @Override
    @Transactional
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    @Override
    @Transactional
    public Optional<Account> getAccount(Long id) {
        return accountRepository.findById(Objects.requireNonNull(id, "id required"));
    }

    @Override
    @Transactional
    public Account addAccount(Account account) {
        logger.info("add account request: account={}", account);

        Objects.requireNonNull(account, "account required");
        Objects.requireNonNull(account.getAmount(), "amount required");

        account.setId(null);
        Account savedAccount = accountRepository.save(account);

        logger.info("add account: account={}", account);
        return savedAccount;
    }

    @Override
    @Transactional
    public Account transfer(Long fromAccountId, Long toAccountId, BigDecimal amount) {
        logger.info("transfer request: from={}, to={}, amount={}", fromAccountId, toAccountId, amount);

        Objects.requireNonNull(fromAccountId, "fromAccountId required");
        Objects.requireNonNull(toAccountId, "toAccountId required");
        Objects.requireNonNull(amount, "amount required");

        ILock firstLock = hazelcastInstance.getLock(ACCOUNT_LOCK_PREFIX + Math.min(fromAccountId, toAccountId));
        ILock secondLock = hazelcastInstance.getLock(ACCOUNT_LOCK_PREFIX + Math.max(fromAccountId, toAccountId));
        try {
            firstLock.lock(10, TimeUnit.SECONDS);
            secondLock.lock(10, TimeUnit.SECONDS);

            Account fromAccount = accountRepository.findById(fromAccountId).orElseThrow(
                    () -> new IllegalStateException("account " + fromAccountId + " not found"));
            Account toAccount = accountRepository.findById(toAccountId).orElseThrow(
                    () -> new IllegalStateException("account " + toAccountId + " not found"));

            TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);

            TransferResult transferResult = transferRequest.execute();

            fromAccount = accountRepository.save(transferResult.getFrom());
            accountRepository.save(transferResult.getTo());
            transferRepository.save(transferResult.getTransfer());
            logger.info("transfer success: from={}, to={}, amount={}, error={}", fromAccountId, toAccountId, amount);
            return fromAccount;
        } catch (Exception e) {
            logger.error("transfer error: from={}, to={}, amount={}, error={}", fromAccountId, toAccountId, amount, e.toString());
            throw e;
        } finally {
            firstLock.unlock();
            secondLock.unlock();
        }
    }

    @Override
    @Transactional
    public List<Transfer> getLastTransfers(Long accountId, Integer count) {
        return transferRepository.findByAccountId(accountId, count);
    }
}
