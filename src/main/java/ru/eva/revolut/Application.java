package ru.eva.revolut;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import io.vertx.core.Launcher;
import ru.eva.revolut.api.MainApiVerticle;
import ru.eva.revolut.guice.MainModule;

public class Application {

    public static void main(String... args) {
        Injector injector = Guice.createInjector(new MainModule());
        injector.getInstance(PersistService.class).start();
        Launcher.executeCommand("run",  MainApiVerticle.class.getName());
    }
}
