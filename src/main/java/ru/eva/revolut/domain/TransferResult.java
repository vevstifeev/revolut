package ru.eva.revolut.domain;

import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;

public class TransferResult {

    private final Account from;

    private final Account to;

    private final Transfer transfer;

    public TransferResult(Account from, Account to, Transfer transfer) {
        this.from = from;
        this.to = to;
        this.transfer = transfer;
    }

    public Account getFrom() {
        return from;
    }

    public Account getTo() {
        return to;
    }

    public Transfer getTransfer() {
        return transfer;
    }
}
