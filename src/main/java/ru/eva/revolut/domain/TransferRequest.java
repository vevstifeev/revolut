package ru.eva.revolut.domain;

import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.domain.tables.pojos.Transfer;
import ru.eva.revolut.util.Assert;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

public class TransferRequest {

    private final Account from;

    private final Account to;

    private final BigDecimal amount;

    public TransferRequest(Account from, Account to, BigDecimal amount) {
        this.from = from;
        this.to = to;
        this.amount = amount.stripTrailingZeros();
    }

    public TransferResult execute() throws IllegalStateException {
        Assert.state(!Objects.equals(from.getId(), to.getId()),
                "transfer to the same account is prohibited");
        Assert.state(from.getAmount().compareTo(amount) >= 0,
                "not enough money for transfer");
        Assert.state(amount.scale() <= 2,
                "scale must be less or equal 2");

        from.setAmount(from.getAmount().subtract(amount));
        to.setAmount(to.getAmount().add(amount));

        return new TransferResult(
                from,
                to,
                new Transfer()
                        .setIdFromAccount(from.getId())
                        .setIdToAccount(to.getId())
                        .setTimestamp(OffsetDateTime.now())
                        .setAmount(amount));
    }
}
