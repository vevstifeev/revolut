package ru.eva.revolut.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import com.hazelcast.core.HazelcastInstance;
import io.vertx.core.Launcher;
import org.junit.*;
import retrofit2.Response;
import ru.eva.revolut.api.MainApiVerticle;
import ru.eva.revolut.domain.tables.pojos.Account;
import ru.eva.revolut.guice.MainModule;
import ru.eva.revolut.test.api.ApiClient;
import ru.eva.revolut.test.api.client.AccountApi;
import ru.eva.revolut.test.api.client.TransferApi;
import ru.eva.revolut.test.api.client.model.ApiAccount;

import java.io.IOException;
import java.math.BigDecimal;

public class TransferServiceTest {

    static Injector injector;

    AccountApi accountApi;

    TransferApi transferApi;

    AccountService accountService;

    Account firstAccount;

    Account secondAccount;

    @BeforeClass
    public static void setUpVertx() {
        injector = Guice.createInjector(new MainModule());
        injector.getInstance(PersistService.class).start();

        Launcher.executeCommand("run", MainApiVerticle.class.getName());
    }

    @AfterClass
    public static void tearDownVertx() {
        injector.getInstance(HazelcastInstance.class).shutdown();
        injector.getInstance(PersistService.class).stop();
    }

    @Before
    public void setUp() {
        accountService = injector.getInstance(AccountService.class);
        BigDecimal firstAccountAmount = BigDecimal.valueOf(100);
        BigDecimal secondAccountAmount = BigDecimal.valueOf(200);
        firstAccount = accountService.addAccount(new Account().setAmount(firstAccountAmount));
        secondAccount = accountService.addAccount(new Account().setAmount(secondAccountAmount));

        ApiClient apiClient = new ApiClient();
        accountApi = apiClient.createService(AccountApi.class);
        transferApi = apiClient.createService(TransferApi.class);
    }

    @Test
    public void transfer_SuccessTest() throws IOException {
        BigDecimal transferAmount = BigDecimal.valueOf(10);
        Response<ApiAccount> response = transferApi
                .transfer(
                        BigDecimal.valueOf(firstAccount.getId()),
                        BigDecimal.valueOf(secondAccount.getId()),
                        transferAmount)
                .execute();

        Assert.assertTrue(response.isSuccessful());

        ApiAccount updatedAccount = response.body();

        Assert.assertEquals(0, firstAccount.getAmount().subtract(transferAmount)
                .compareTo(updatedAccount.getAmount()));
        Assert.assertEquals(0, firstAccount.getAmount().subtract(transferAmount)
                .compareTo(accountService.getAccount(firstAccount.getId()).get().getAmount()));
        Assert.assertEquals(0, secondAccount.getAmount().add(transferAmount)
                .compareTo(accountService.getAccount(secondAccount.getId()).get().getAmount()));
    }

    @Test
    public void transfer_FailedIfSameAccountTest() throws IOException {
        BigDecimal transferAmount = BigDecimal.valueOf(10);
        Response<ApiAccount> response = transferApi
                .transfer(
                        BigDecimal.valueOf(firstAccount.getId()),
                        BigDecimal.valueOf(firstAccount.getId()),
                        transferAmount)
                .execute();
        Assert.assertFalse(response.isSuccessful());
        Assert.assertEquals("transfer to the same account is prohibited", response.message());
    }

    @Test
    public void transfer_FailedIfNotEnoughMoneyTest() throws IOException {
        Response<ApiAccount> response = transferApi
                .transfer(
                        BigDecimal.valueOf(firstAccount.getId()),
                        BigDecimal.valueOf(secondAccount.getId()),
                        firstAccount.getAmount().add(BigDecimal.valueOf(1)))
                .execute();
        Assert.assertFalse(response.isSuccessful());
        Assert.assertEquals("not enough money for transfer", response.message());
    }

    @Test
    public void transfer_FailedAmountScaleMoreThanTwoTest() throws IOException {
        BigDecimal transferAmount = new BigDecimal("10.123");
        Response<ApiAccount> response = transferApi
                .transfer(
                        BigDecimal.valueOf(firstAccount.getId()),
                        BigDecimal.valueOf(secondAccount.getId()),
                        transferAmount)
                .execute();
        Assert.assertFalse(response.isSuccessful());
        Assert.assertEquals("scale must be less or equal 2", response.message());
    }
}
